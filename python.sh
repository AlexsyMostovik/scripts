url=https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tar.xz
filename=$(cat | rev | cut -d / -f1 $url | rev)
directory=$(echo $filename | cut -d . -f 1)



sudo wget -c $url
sudo tar -xvf $filename
sudo cd $directory/
sudo ./configure
sudo make 
sudo make test
sudo make install